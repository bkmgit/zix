..
   Copyright 2020-2022 David Robillard <d@drobilla.net>
   SPDX-License-Identifier: ISC

Zix is a lightweight C library of portability wrappers and data structures.
